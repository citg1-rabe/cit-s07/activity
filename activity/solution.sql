-- 1 create DATABASE blog_db
CREATE DATABASE blog_db;

-- create user table
CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(100) NOT NULL,
password VARCHAR(300) NOT NULL,
datetime_created DATETIME NOT NULL,
PRIMARY KEY(id)
);

-- create posts table
CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
author_id INT NOT NULL,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_posted DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_posts_user_id
    FOREIGN KEY (author_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

-- create post_comments table
CREATE TABLE post_comments(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_commented DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_comments_user_id
    FOREIGN KEY (user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_post_comments_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

-- create post_likes table
CREATE TABLE post_likes(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_likes_user_id
    FOREIGN KEY (user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_post_likes_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);



--2 insert record to user table.

INSERT INTO users( email, password, datetime_created) 
	Values ("johnsmith@gmail.com", "passwordA","2021-01-01 01:00:00");

INSERT INTO users( email, password, datetime_created) 
	Values ("juandelacruz@gmail.com", "passwordB","2021-01-01 02:00:00");

INSERT INTO users( email, password, datetime_created) 
	Values ("janesmith@gmail.com", "passwordC","2021-01-01 03:00:00");

INSERT INTO users( email, password, datetime_created) 
	Values ("mariadelacruz@gmail.com", "passwordD","2021-01-01 04:00:00");

INSERT INTO users( email, password, datetime_created) 
	Values ("johndoe@gmail.com", "passwordE","2021-01-01 05:00:00");	

-- 3 insert record to table posts.

INSERT INTO posts(author_id, title, content, datetime_posted) 
	VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00"),
 	(1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00"),
  	(2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00"),
    (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

-- 4 get all the post with author id =1.

SELECT * FROM posts where author_id =1;

-- 5 get all user mail and date time of creation.

SELECT email, datetime_created FROM users;

-- 6 update posts content to "Hello to the people of the Earth!"
  UPDATE posts 
  SET content = "Hello to the people of the Earth!" 
  WHERE id = 2;

-- 7 Delete the user with an email of johndoe@gmail.com
DELETE FROM users where email = "johndoe@gmail.com";